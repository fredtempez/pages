Ouvrez l’image ***recadrer-rottweiler-7510724_1920*** 

Les dimensions de cette image sont raisonnables (1920 x 1280), nous n’y toucherons pas, en revanche, le cadrage n’est pas bon, il pourrait être amélioré.

La construction d’une image obéit à des critères de mise en scène et notamment à la règle des 1/3. L’image est symboliquement divisée en 3 lignes horizontales et verticales. 
![](regles_des_tiers.jpg)

Les intersections de ces lignes imaginaires forment des points de force.  La composition de l’image consiste à respecter des lignes et ces points de force. Cliquez [ce lien](https://www.agence-equation.com/photographie/6-regles-cadrage-composition-photo/) pour en apprendre plus.

Lorsque l’image ne respecte pas ces règles de composition, on utilise le recadrage afin d’améliorer la mise en scène.

## Comment recadrer :

Dans la barre latérale, la flèche doit être sélectionnée, tracez un cadre dans l’image. La tête du chien est volontairement placée en haut à gauche afin de donner du champ à son regard et de la positionner à l’intersection des deux lignes de force (1/3 de l’image).
![](rottweiller.png)

## Mode opératoire 1

- Pour étendre ou réduire la zone sélectionnée, cliquez sur les bords de la sélection.
- Pour annuler la sélection, appuyez sur la touche **Échapp** du clavier.
- Pour que la sélection respecte la proportion de l’image originale, cliquez sur le menu **Sélection** puis **Adapter le rappor**t (on peut aussi utiliser le bouton dans la palette des outils à droite de l’écran). Pour cette image, le rapport est de 3:2 (largeur / hauteur 1,5)

![](adapater_rapport.png)

- Pour appliquer le recadrage :
    - Cliquez le menu **Image** puis **Recadrer**
    - ou  clic droit dans la sélection puis **Recadrer l’image**.

Il ne reste plus qu’à enregistrer l’image en JPG ***rottweiler-1***

## Mode opératoire 2

Téléchargez l’image ***recadrer-bird-7466310_1920***

Les dimensions élevées de cette image permettent un **recadrage** serré sans perte de qualité, isolez l’oiseau du reste de l’image pour obtenir une image de 2000 pixels de largeur.

Pour créer une sélection de la dimension originale de l’image :

1. Cliquez sur le menu **Sélection** puis sur **Paramétrage manuel**, 
2. **Conserver les proportions** est coché.
3. Saisir 2000 pixels de largeur.
4. La sélection s’affiche, il suffit de la déplacer.
5. Clic droit puis **Recadrer**.
![](bird_petit.png)

## Exercice

Ouvrez l’image ***recadrer-andrea-beltran-13834229***

Cette image est orientée en portrait, la hauteur est plus grande que la largeur. Elle a certainement été capturée à l’aide d’un téléphone portable.

L’objectif est de la recadrer au format paysage : 

- Le sujet principal est la cabane.
- On souhaite conserver la plus grande largeur.
- La hauteur de la sélection est au rapport 4:3 (exemple largeur 400, hauteur 300), utilisez l’outil d’adaptation du rapport.

Image obtenue, sa taille est 3718 x 2789 pixels :

![](cabane.png)
Réduisez sa taille à 1600 pixels de large, avant de l’enregistrer.

## En résumé

- Le recadrage permet de découper une image au cutter.
- L’image découpée est toujours plus petite que l’image originale, elle contient moins de pixels.
- On ne recadre pas trop fortement une image de faible dimension.