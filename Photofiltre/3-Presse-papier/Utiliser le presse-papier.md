Photofiltre sait ouvrir plusieurs images en même temps ; chacune dans un onglet, ouvrez les deux images 

- ***colle-sleeping-girl-g6e705d810_1920*** et
- ***colle-roses-gfba72ad79_1920***

Le projet est d’ajouter une rose blanche dans le bouquet de l’image avec la personne allongée.

# Copie à partir de l’image source

Cliquez sur la fenêtre de l’image contenant les roses. Vous remarquez un damier gris et blanc à l’arrière-plan, cela indique que le fond est transparent, ainsi, il ne sera pas nécessaire de détourer la sélection pour éliminer l’arrière-plan.

## Mode opératoire

1. Dans la barre d’outils à droite, l’outil de sélection (une flèche) est activé.
2. Tracez une sélection autour de la rose blanche qui est en bas à droite.
3. D’un clic droit dans la sélection ou dans le menu **Édition**, cliquez sur **Copier**.
4. L’image est transférée dans la mémoire de Photofiltre.

# Coller dans l’autre image

Cliquez sur l’onglet de la seconde image, puis d’un clic droit sur le fond de l’image et **Coller** ou à l’aide du menu **Édition**, cliquez sur **Coller**.

## Déplacement de la sélection

1. La sélection est insérée dans l’image dans un cadre appelé calque. 
2. Pour la déplacer, cliquez à l’aide du bouton gauche dans le cadre (ici en rouge), maintenez et déplacez le cadre au-dessus du vase.
3. La copie est superposé au-dessus du fond.

![](femme_couchee_selection_rouge.png)

# Transformer la sélection avant de la copier

Pour épaissir le bouquet, on ajoutera une autre rose blanche, plus petite avec un effet miroir. Pour ce faire, on peut passer par une étape intermédiaire en collant la sélection dans une nouvelle image.

## Mode opératoire

### Copie dans une nouvelle image

1. Cliquez sur l’image avec les fleurs.
2. Sélectionnez à nouveau la rose blanche en bas à droite
3. Clic droit sur la sélection **Copier**
4. Dans le menu **Édition**, cliquez sur **Coller en tant qu’image**.

### Effet miroir

Cliquez sur le bouton de symétrie

![](symétrie.png)

### Réduction de la dimension de l’image

La nouvelle largeur est de 300 pixels, ne pas déformer la hauteur.

### Coller l’image modifiée

Pour sélectionner la totalité de l’image dans le menu **Sélection**, cliquez sur **Tout sélectionner** ou tapez le raccourci CTRL-A.

Ensuite, copiez la sélection à l’aide du menu **Édition / Copier** ou **CTRL-C** ou clic droit sur l’image **Copier.**

Enfin, sélectionnez l’image avec le vase, collez puis déplacez la sélection.

### Les calques

Les sélections sont insérées dans des **calques qui sont visibles** dans une barre à gauche de l’image.

L’ordre des calques conditionne la priorité d’affichage du premier plan vers l’arrière. Pour le modifier l’ordre, glissez les icônes des calques ou clic droit puis **Ordre** sur un calque.

![](barre_calques.png)

Pour fusionner le calque dans le fond, effectuez un clic droit sur le fond de la sélection ou du calque et cliquez sur **Fusionner avec le calque inférieur**. La copie est alors définitive, il n’est plus possible de déplacer les calques.

![](femme_couchée_vase.png)

# Exercice 1

Ajoutez une autre rose en réduisant sa dimension et en appliquant un effet miroir.

![](femme_couchee_vase_fait.png)

# Exercice 2

Téléchargez les deux images dans le dossier pub.

L’image selfie est déjà détourée.

Insérez cette image dans l’image du pub au niveau de la fenêtre en changeant les dimensions du calque.

Redimensionnez l’image du pub, le rapport d’image recadrée est 3:2

![](pub.png)