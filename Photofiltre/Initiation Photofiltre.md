Ce logiciel est un outil simple de retouche de photo. Il est installé à l’université, pour l’installer sur votre ordinateur personnel, suivre ce lien [](http://www.photofiltre-studio.com/pf7.htm)[http://www.photofiltre-studio.com/pf7.htm](http://www.photofiltre-studio.com/pf7.htm)
La version 7 est gratuite, c’est celle que nous utiliserons. Recherchez l’icône Photofiltre dans le menu Démarrer et lancez-le...


## Dimension des images
>Les images téléchargées sur le net ou les photos prises avec un téléphone sont souvent trop grandes pour être utilisées nativement dans une présentation ou un document écrit. Il est donc nécessaire de réduire la dimension ainsi que le poids.

[Réduire la dimension et le poids d’une image](https://fredtempez.codeberg.page/Photofiltre/1-Dimension/dimension.html)


## Recadrer une image
>Le recadrage consiste à changer la place du sujet principal dans l’image. Pour ce faire, on découpe l’image pour ne garder que le sujet.

[Recadrer une image](https://fredtempez.codeberg.page/Photofiltre/2-Recadrer/recadrer.html)

## Le presse-papier
>Dans le cas d’un photomontage ou dans l’incrustation d’une vignette dans une image, on utilise le presse-papier pour copier une partie d’image et la coller dans une autre image.

[Utiliser le presse-papier](https://fredtempez.codeberg.page/Photofiltre/3-Presse-papier/presse-papier.html)

## Du texte dans une image
>Pour concevoir un mème, il faut tout d’abord trouver la bonne image parmi [celles qui sont souvent utilisées](https://www.pinterest.fr/mimilifestyle/m%C3%A8mes/) et d’y ajouter du texte. Pour cela, on a besoin d’insérer du texte dans l’image et aussi de gérer le format d’enregistrement des calques.

[Concevoir un meme](https://fredtempez.codeberg.page/Photofiltre/4-Texte/meme.html)
