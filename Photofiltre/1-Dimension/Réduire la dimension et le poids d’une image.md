Chargez les images sur le cours Moodle dans votre dossier image.

Dans Photofiltre, Fichier / Ouvrir l'image ***redimensionner-mountains-7480902**.* On peut aussi glisser l'image dans la fenêtre Photofiltre.

Dans la barre de statut, la dimension de l'image est indiquée en pixels suivie du nombre de couleurs :

![](barre_status.png)

C'est une très grande taille, bien trop grande pour une présentation ou un document écrit. Le risque est de grossir inutilement le poids du fichier et de rendre difficile sa copie sur une clé ou dans le cloud.

Néanmoins, il faut conserver le fichier original afin de pouvoir l'imprimer en haute qualité.

Comment la réduire et quel format d'image choisir pour gagner du poids ?

## Naviguer dans l'image.

La roulette de la souris permet de zoomer ou de dézoomer, les ascenseurs sur les bords de se déplacer dans l'image.

L'outil Main permet de se déplacer dans l'image lorsque le zoom est important. Activez l'outil puis cliquez dans l'image en maintenant la pression puis déplacez la souris.

![](outil_main.png)

## Comment adapter la dimension de l'image ?

1.  Cliquez sur le menu **Image** puis **Taille de l'image**.
2.  Les dimensions sont indiquées en **Largeur** et en **Hauteur**.
3.  L'unité de mesure est le **pixel**. On peut changer ce champ en pourcentages, dans ce cas, il est plus facile de diminuer ou d'augmenter la taille de l'image :
    1.  diviser par deux : 50%
    2.  doubler la taille : 200%.
4.  La case **Conserver les proportions** évite une déformation de l'image, elle doit être cochée.
5.  Saisir la nouvelle largeur 1024 pixels, la hauteur est recalculée automatiquement 683 pixels.
6.  Autres options :
    1.  Le **rééchantillonnage** permet de choisir l'algorithme de traitement. Cette option est utile pour l'agrandissement d'une image trop petite, le choix de l'algorithme une incidence sur la qualité du résultat.
    2.  Pour les dimensions **en centimètres ou en pouces**, il faut préciser la densité de pixels que l'on appelle la résolution. Elle doit être de 300 pixels pour une image imprimée et de 75 pour une image affichée sur un écran ou un vidéoprojecteur.
7.  Cliquez Ok
8.  La nouvelle dimension est désormais modifiée dans la barre de statut.

## La qualité de l'image est réduite.

En zoomant dans l'image, vous visualisez des carrées, ce sont des **pixels**, la qualité de l'image a été dégradée, il faudra donc l'enregistrer sous un nouveau nom et ne pas écraser l'image originale.


**Agrandir la taille d'une image** | **Réduire la taille d'une image**
---|---
C'est inventer de nouveaux pixels| C'est détruire des pixels

Dans les deux cas, la qualité de l'image sera plus ou moins dégradée.

## Enregistrer dans un nouveau format.

### En JPG

Cliquez sur Enregistrer sous, changez le format d'enregistrement en JPG, nommez l'image ***mountains-2*** et validez.

Une nouvelle fenêtre apparaît, une réglette permet de modifier le facteur de compression :

| Qualité inférieure | Qualité supérieure |
|--------------------|--------------------|
| Poids petit        | Poids élevé        |

![](compression.png)

En cliquant sur le bouton **Aperçu**, le poids de l'image enregistrée est affichée et les effets de la compression sur l'image sont visibles.

Pour en tester les effets, zoomez sur l'image sans quitter cette fenêtre, déplacez progressivement la réglette vers une qualité inférieure puis cliquez sur **Aperçu** : l'image se dégrade progressivement mais son poids en Kilo octets diminue.

Enfien, sélectionnez 90% et validez avec Ok.

### En PNG

Cliquez sur Enregistrer sous, le format par défaut est PNG, nommez l'image ***mountains-1*** et sélectionnez le même dossier que précédemment. Vous pouvez valider l'écran qui s'affichera sans modifier les options.

## Effets sur les poids ?

Ouvrez le dossier dans lequel vous avez enregistré les deux images, changez l’affichage style Détails et classez les images par poids en cliquant sur l’en-tête Taille.

À dimension équivalente, le format JPG est meilleur que le format PNG en termes de réduction du poids de l'image. On dit que le format JPG dispose d'**une compression plus efficace**.

## En résumé

Pour réduire le poids d'une image avant de l'insérer dans un document :

- Je choisis une **largeur** parmi celles définies par le standard et je coche l'option pour conserver les proportions.

![](norme_vga.png)

- J'utilise le format **JPG** de préférence.
- J'enregistre sous un autre nom pour ne pas perdre l'image originale.

# Exercice

1.  Ouvrez l'image ***redimensionner-outdoor-6615901***
2.  Modifiez la largeur en 1024 pixels.
3.  Enregistrez l'image en JPG sous le nom ***outdoor-1,*** le poids de l'image doit être inférieur à 200 ko. Pour cela, cliquez sur le bouton **Aperçu**, ce qui affichera le poids de l'image et appliquera la compression à l'image affichée.

# Agrandir une image pour l'imprimer

Lorsqu'une image est trop petite, elle ne peut pas être imprimée, en effet, l'impression nécessite beaucoup de pixels. L'agrandissement d'une image consiste à inventer des pixels, pour cela il faut un logiciel qui soit capable d'imaginer de nouveaux pixels, c'est un algorithme de **rééchantillonnage**.

Il existe quelques algorithmes de **rééchantillonnage** dans Photofiltre, selon le type d'image et le facteur de l'agrandissement souhaité, l'efficacité dépendra du choix de l'algorithme :

![](rééchantillonnage.png)

Comment procéder ?

1.  Ouvrez l'image **agrandir-fascinating-houses-get-ideas-very-small-house-plans_319254.jpeg**
2.  Dupliquez l'image en utilisant le menu **Image** puis **Dupliquer**.
3.  Cliquez sur la fenêtre de l'image dupliquée.
4.  Cette image est orientée en portrait et non en paysage. Elle est bien trop petite pour être imprimée avec une qualité suffisante.
5.  La hauteur minimale pour une impression de qualité est de 2408 pixels, veillez à ce que la case de conservation des proportions est cochée puis saisir 2408 pixels dans la case hauteur.
6.  Sélectionnez un algorithme de **rééchantillonnage** parmi la liste, et validez l'écran. Zoomez sur les deux images pour juger de la qualité du résultat.
7.  Pour tester un autre algorithme, cliquez sur la fenêtre de l'image originale et reprenez au point numéro 2 (ne pas tester tous les algorithmes 😇 )

Quel algorithme a donné le meilleur résultat ? Enregistrez l'image que vous avez sélectionnée sous le nom **fascinating-houses-big.**
