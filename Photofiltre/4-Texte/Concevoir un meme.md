À partir d’une image, on ajoute du texte afin d’exprimer un message simple, exemple :

![](meme.png)

## Insertion du calque texte

Recherchez et cliquez l’icône T dans la barre d’outils. Une fenêtre apparaît. Pour un mème, on choisira : 

- Dans l’onglet texte, 
	- Une police serif dont la taille est comprise entre 20 et 30 pixels,
	- Couleur du texte blanc
- Dans l’onglet effet, 
	- Un contour noir.

Validez.

## Déplacement et édition du texte

Le texte est un calque, pour le déplacer, cliquez dans une lettre, maintenez et déplacez la souris.

Pour éditer le texte, double clic dans une lettre, la fenêtre d’édition apparaît à nouveau.

## Enregistrement dans un format éditable

Pour publier le mème, on enregistre l’image au format PNG ou JPEG mais cela aura pour effet **de supprimer les calques**, il ne sera alors plus possible d’éditer les textes.

Pour conserver une copie contenant les calques, sélectionnez le format d’enregistrement d’image **PFI** qui est un format interne de Photofiltre. Malheureusement, ce format est connu seulement de Photofiltre, pour publier le même, il faut choisir impérativement le format PNG ou JPEG.

# Exercice

Des images de même sont proposées sur Moodle, à vous de laisser libre cours à votre imagination.